from persona import Persona

class Empleado(Persona):    #persona= superclase     empleado= subclase
    sueldo= 0

    def __init__(self,nombre,apellido,sueldo):
        super().__init__(nombre,apellido)   #super= constructor de la clase Persona
        self.sueldo = sueldo

    
    def get_sueldo(self):
        return self.sueldo

Empleado1= Empleado("Pablo","Navas",100000)     #Empleado1= objeto.    Empleado= clase     #instanciar= crear un objeto para poder usar sus atributos y metodos

print(Empleado1.get_telefono())
print(Empleado1.get_sueldo())
