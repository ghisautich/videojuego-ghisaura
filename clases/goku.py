import pygame


class Goku(pygame.sprite.Sprite):
    
    #ATRIBUTOS
    spritesheet_x=None
    frames_derecha={}
    frames_izquierda={}
    frame=0

    def __init__(self):
        
        super().__init__()

        self.spritesheet_x = pygame.image.load("assets/spritesheets/Goku_modificado.png").convert()

        self.spritesheet_x.set_clip(pygame.Rect(0,0, 48, 104))
        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())

        self.frames_derecha = { 0:(13,445, 54, 69), 
                               1:(87, 449, 72, 65), 
                               2:(174, 457, 54, 59), 
                               3:(248, 453, 77, 60) }
        
        self.frames_izquierda = { 0:(0, 0, 32, 48),
                                  1: (32, 0, 32, 48),
                                  2: (64, 0, 32, 48),
                                  3: (96, 0, 32, 48)
                                 }
        
        self.rect = self.image.get_rect()
        self.rect.x = 100
        self.rect.y = 200


    def devolver_frame(self, animacion):
       
        self.frame += 1
        if self.frame >= len(animacion):
            self.frame = 0
        
        return animacion[self.frame]


    def clip(self, animacion):
        self.spritesheet_x.set_clip(pygame.Rect(self.devolver_frame(animacion)))
     
    

    def update(self, direccion):

       
        
        if direccion == "derecha":
            #self.clip(self.frames_derecha)
            
            self.clip(self.frames_derecha)

            self.rect.x += 10 

        elif direccion== "izquierda":
            self.clip(self.frames_izquierda)
            self.rect.x -= 10

        if direccion == "abajo":
            #self.clip(self.frames_derecha)

            self.rect.y += 10 

        if direccion == "arriba":
            #self.clip(self.frames_derecha

            self.rect.y -= 10 


    def administrar_eventos(self,event):
        if pygame.key.get_pressed()[pygame.K_d]:
            self.update("derecha")
        elif pygame.key.get_pressed()[pygame.K_a]:
            self.update("izquierda")
        

        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())