import pygame


class Marciano(pygame.sprite.Sprite):
    
    #ATRIBUTOS
    spritesheet_x=None  #Referencia al archivo del spritesheet ("png")
    velocidad= 10       #Determina la cantidad de pixeles que se desplaza el spritesheet
    frames_derecha={}   #Cada uno de los frames de la animación
    frames_izquierda={} #       ''
    frame=0             #Posición de cada frame en un momento determinado

    def __init__(self):         #Constructor
        
        super().__init__()      #Constructor de la clase padre

        self.spritesheet_x = pygame.image.load("assets/spritesheets/marciano.png").convert()

        self.spritesheet_x.set_clip(pygame.Rect(0,0, 32, 48))
        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())

        self.frames_derecha = { 0:(160, 0, 32,48), 
                               1:(192, 0, 32,48), 
                               2:(224, 0, 32,48), 
                               3:(256, 0, 32,48) }
        
        self.frames_izquierda = { 0:(0, 0, 32, 48),
                                  1: (32, 0, 32, 48),
                                  2: (64, 0, 32, 48),
                                  3: (96, 0, 32, 48)
                                 }
        
        self.rect = self.image.get_rect()
        self.rect.x = 100
        self.rect.y = 200


    def devolver_frame(self, animacion):
       
        self.frame += 1
        if self.frame >= len(animacion):
            self.frame = 0
        
        return animacion[self.frame]


    def clip(self, animacion):
        self.spritesheet_x.set_clip(pygame.Rect(self.devolver_frame(animacion)))
     
    

    def update(self, direccion):

       
        
        if direccion == "derecha":
            #self.clip(self.frames_derecha)
            
            self.clip(self.frames_derecha)

            self.rect.x += self.velocidad

        elif direccion== "izquierda":
            self.clip(self.frames_izquierda)
            self.rect.x -= self.velocidad

        if direccion == "abajo":
            #self.clip(self.frames_derecha)

            self.rect.y += self.velocidad

        if direccion == "arriba":
            #self.clip(self.frames_derecha

            self.rect.y -= self.velocidad 


    def administrar_eventos(self,event):
        if event.type==pygame.KEYDOWN:

            if event.key==pygame.K_RIGHT:
                self.update("derecha")

            elif event.key==pygame.K_LEFT:
                self.update("izquierda")

            if event.key==pygame.K_DOWN:
                self.update("abajo")

            if event.key==pygame.K_UP:
                self.update("arriba")
        

        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())
            
        