import pygame
import sys
from clases.personaje import Marciano
from clases.goku import Goku


pygame.init()

ventana = pygame.display.set_mode((500,479))

reloj = pygame.time.Clock()

marciano = Marciano()

goku = Goku()


while True:     #Bucle de "Juego"
    ''' Esto significa que se van realizan 30
        actualizaciones del juego por segundo.
        Es necesario hacerlo en cada iteración
        por que si no se reinicia
    '''
    ventana.fill((18,90,200))

    for event in pygame.event.get():    #Cuando ocurre un evento...
        
        #print("EVENTO: ", pygame.key.get_pressed())
        if event.type == pygame.QUIT:   #Si el evento es cerrar la ventana
            pygame.quit()        #Se cierra pygame
            sys.exit()           #Se cierra el programa
   
        if event.type == pygame.KEYDOWN:
            #print("Apreto tecla: ", event.key)
            if event.key == pygame.K_RIGHT:
                pass#marciano.update("derecha")

    if marciano.rect.x >=500:
            marciano.rect.x = 0


    if marciano.rect.x < 0:
        marciano.rect.x = 500

    if marciano.rect.y >=479:
            marciano.rect.y = 0


    if marciano.rect.y < 0:
        marciano.rect.y = 479

    if marciano.rect.y <= 410:
        marciano.rect.y += 5

    ventana.blit(marciano.image,marciano.rect)
    ventana.blit(goku.image,goku.rect)
    marciano.administrar_eventos(event)
    goku.administrar_eventos(event)  

    pygame.display.update()
    

    reloj.tick(30)